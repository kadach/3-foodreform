(function ($) {
    $(function () {
        $(window).load(function () {
            var reviewsWrap = $('.reviews .reviews-wrap');
            reviewsWrap.css('height', reviewsWrap.height() + "px");

            var menuload = $($('.one-menu-result')[0]).height();
            $('.menu-wrap-result').css('height', menuload + "px");

            $('.menu-result').css('height',$('.menu-result').height()+"px")
        });
        $(window).resize(function () {
            var reviewsWrap = $('.reviews .reviews-wrap');
            reviewsWrap.css('height', "auto");
            reviewsWrap.css('height', reviewsWrap.height() + "px");
        });



        //SCROLL
        $('[data-go]').click(function () {
            var dataGo = $($(this).attr('data-go')).offset().top;
            $('body').animate({
                scrollTop: dataGo
            }, 1000);
        });

        //POPUP TELL
        var btnPopUpTell = $('[data-popup-call]');
        var closePopUpTell = $('.popup-call .close');
        btnPopUpTell.click(function () {
            var popupCall = $($(this).attr('data-popup-call'));
            popupCall.click(function () {
                if ($(event.target).hasClass('popup-call')) {
                    popupCall.removeClass('see');
                    setTimeout(function () {
                        popupCall.removeClass('life');
                    }, 430)
                }
            });
            popupCall.addClass('life');
            setTimeout(function () {
                popupCall.addClass('see');
            }, 40);
            closePopUpTell.click(function () {
                popupCall.removeClass('see');
                setTimeout(function () {
                    popupCall.removeClass('life');
                }, 430)
            });
        });

        //POPUP COMMIT
        var btnPopUpMass = $('[data-popup-comm]');
        var closePopUpMass = $('.popup-comm .close');
        btnPopUpMass.click(function () {
            var popupCall = $($(this).attr('data-popup-comm'));
            popupCall.click(function () {
                if ($(event.target).hasClass('popup-comm')) {
                    popupCall.removeClass('see');
                    setTimeout(function () {
                        popupCall.removeClass('life');
                    }, 430)
                }
            });
            popupCall.addClass('life');
            setTimeout(function () {
                popupCall.addClass('see');
            }, 40);
            closePopUpMass.click(function () {
                popupCall.removeClass('see');
                setTimeout(function () {
                    popupCall.removeClass('life');
                }, 430)
            });
        });


        //SORT FOR MENU
        var sortA;
        if($(window).width()>= 768){
            sortA = {
                btn: $('[data-open-menu]'),
                btnClass: "",
                allResult: $('.all-result')
            };
        } else {
            sortA = {
                btn: $('.mob-menu-btn'),
                btnClass: "",
                allResult: $('.all-result')
            };
        }
        sortA.btn.click(function () {
            sortA.btnClass = $($(this).attr('data-open-menu'));
            sortA.allResult.addClass('close');
            setTimeout(function () {
                sortA.allResult.addClass('die');
                sortA.btnClass.removeClass('die');
                setTimeout(function () {
                    sortA.btnClass.removeClass('close');
                },40);
            },440);
        });

        var sortM = {
            btn: $('[data-menu]'),
            btnNumber: "",
            parent: "",
            children: "",
            next: ""
        };
        sortM.btn.click(function () {
            sortM.btnNumber = $(this).attr('data-menu') * 1;
            sortM.parent = $(this).closest('.btn-wrap');
            sortM.next = sortM.parent.siblings('.menu-wrap-result');
            sortM.children = sortM.next.children('.one-menu-result');
            sortM.children.addClass('close');
            setTimeout(function () {
                setTimeout(function () {
                    sortM.children.addClass('die');
                    $(sortM.children[sortM.btnNumber]).removeClass('die');
                    setTimeout(function () {
                        $(sortM.children[sortM.btnNumber]).removeClass('close');
                    }, 40)
                }, 440);
            }, 40)
        });

        //OPEN COMENT
        var addReviewBtn = $('[data-open-reviews]');
        addReviewBtn.click(function () {
            var reviewsWrap = $('.reviews .reviews-wrap');
            var oneReviews = $('.reviews-wrap .col-sm-6');
            var allCloseReview = $('.one-reviews.die');
            if ($(window).width() >= 991) {
                if (allCloseReview.length > 4) {
                    reviewsWrap.css('height', (reviewsWrap.height() + oneReviews.height()) + "px");
                    allCloseReview.slice(0, 4).removeClass('die');
                    setTimeout(function () {
                        allCloseReview.slice(0, 4).removeClass('close');
                    }, 40);
                } else {
                    reviewsWrap.css('height', (reviewsWrap.height() + oneReviews.height()) + "px");
                    allCloseReview.removeClass('die');
                    setTimeout(function () {
                        allCloseReview.removeClass('close');
                    }, 40);
                    addReviewBtn.css('display', 'none');
                }
            } else if ($(window).width() >= 768 && $(window).width() > 991) {
                if (allCloseReview.length > 4) {
                    reviewsWrap.css('height', (reviewsWrap.height() + oneReviews.height() * 2) + "px");
                    allCloseReview.slice(0, 4).removeClass('die');
                    setTimeout(function () {
                        allCloseReview.slice(0, 4).removeClass('close');
                    }, 40);
                } else {
                    reviewsWrap.css('height', (reviewsWrap.height() + oneReviews.height() * 2) + "px");
                    allCloseReview.removeClass('die');
                    setTimeout(function () {
                        allCloseReview.removeClass('close');
                    }, 40);
                    addReviewBtn.css('display', 'none');
                }
            } else {
                if (allCloseReview.length > 4) {
                    allCloseReview.slice(0, 4).removeClass('die');
                    reviewsWrap.css('height', (reviewsWrap.height() + $(allCloseReview[0]).parent().height() + $(allCloseReview[1]).parent().height() + $(allCloseReview[2]).parent().height() + $(allCloseReview[3]).parent().height()) + "px");
                    setTimeout(function () {
                        allCloseReview.slice(0, 4).removeClass('close');
                    }, 40);
                } else {
                    allCloseReview.removeClass('die');
                    reviewsWrap.css('height', (reviewsWrap.height() + $(allCloseReview[0]).parent().height() + $(allCloseReview[1]).parent().height() + $(allCloseReview[2]).parent().height() + $(allCloseReview[3]).parent().height()) + "px");
                    setTimeout(function () {
                        allCloseReview.removeClass('close');
                    }, 40);
                    addReviewBtn.css('display', 'none');
                }
            }
        });
    });

//SLIDER FOR MENU
    if ($(window).width() < 768 && $(window).width() >= 500) {
        $('.all-tabs').slick({
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1
        });
    } else if ($(window).width() < 500) {
        $('.all-tabs').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1
        });
    }
})(jQuery);